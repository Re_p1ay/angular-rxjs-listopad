import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { DefaultValueAccessor, ControlValueAccessor, NgForm } from '@angular/forms';
import { Playlist } from 'src/app/core/model/playlist';

// ControlValueAccessor
// DefaultValueAccessor

@Component({
  selector: 'app-playlist-edit-form',
  templateUrl: './playlist-edit-form.component.html',
  styleUrls: ['./playlist-edit-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush // ngOnChanges
})
export class PlaylistEditFormComponent implements OnInit {

  @Input() playlist!: Playlist
  draft!: Playlist

  @Output() cancel = new EventEmitter();
  @Output() save = new EventEmitter<Playlist>();

  constructor(private cdr: ChangeDetectorRef) {
    console.log('constructor')
  }

  saveSubmit(form: NgForm) {
    const draft: Playlist = {
      ...this.playlist,
      name: form.value.name,
      ...form.value.details
    }
    this.save.emit(draft)
  }

  cancelClicked() {
    this.cancel.emit()
  }
  details = true

  @ViewChild('formRef', { read: NgForm, static: true })
  formRef?: NgForm

  ngOnInit(): void {
    console.log('ngOnInit')
    // this.draft = JSON.parse(JSON.stringify(this.playlist))
    // this.draft = { ...this.playlist }

    setTimeout(() => {
      this.formRef?.setValue({
        name: this.playlist.name,
        details: {
          public: this.playlist.public,
          description: this.playlist.description,
        }
      })
    })
  }

  ngDoCheck(): void {
    console.log('ngDoCheck')
    //Called every time that the input properties of a component or a directive are checked. Use it to extend change detection by performing a custom check.
    //Add 'implements DoCheck' to the class.
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
  }

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    console.log('ngOnChanges', changes)
    this.draft = { ...this.playlist }
  }

  ngAfterViewChecked(): void {
    //Called after every check of the component's view. Applies to components only.
    //Add 'implements AfterViewChecked' to the class.
    console.log('ngAfterViewChecked')
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    console.log('ngOnDestroy')
  }
}
