import { Injectable } from '@angular/core';
import { OAuthService, AUTH_CONFIG } from 'angular-oauth2-oidc';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  token = '';

  getToken() {
    return this.oAuth.getAccessToken()
  }

  async login() {
    await this.oAuth.initLoginFlowInPopup()
    console.log(this.oAuth.getAccessToken())
  }

  logout(){
    this.oAuth.logOut()
  }

  async init() {
    this.oAuth.configure(environment.authImplicitFlowConfig)

    await this.oAuth.tryLogin({})
    if (!this.oAuth.hasValidAccessToken()) {
      // this.oAuth.initLoginFlow()
      // this.oAuth.initLoginFlowInPopup() // Popup blocker
    }
    this.token = this.getToken()
    console.log(this.token)
  }

  constructor(private oAuth: OAuthService) {

  }
}
