import { EventEmitter, Inject, Injectable } from '@angular/core';
import { Playlist, PlaylistsSource } from '../../model/playlist';
import { PLAYLISTS_DATA } from '../../tokens';

@Injectable({
  // providedIn: CoreModule
  providedIn: 'root'
})
export class PlaylistService implements PlaylistsSource {

  playlistsChange = new EventEmitter<Playlist[]>()

  constructor(
    @Inject(PLAYLISTS_DATA) private playlists: Playlist[]
  ) { }

  getPlaylistById(selectedId: string | undefined): Playlist | undefined {
    return this.playlists.find(p => p.id === selectedId)
  }

  getPlaylists() {
    return this.playlists
  }

  savePlaylist(draft: Playlist) {
    this.playlists = this.playlists.map(p => p.id === draft.id ? draft : p)

    this.playlistsChange.emit(this.playlists)
  }

}
