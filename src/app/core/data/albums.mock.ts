import { Album } from '../model/album';
import { Playlist } from '../model/playlist';

export const albumsMock = [
  {id: '123', name: 'Album 123', images: [{url:'http://lorempixel.com/300/300'}] as { url: string }[]},
  {id: '234', name: 'Album 234', images: [{url:'http://lorempixel.com/300/300'}] as { url: string }[]},
  {id: '345', name: 'Album 345', images: [{url:'http://lorempixel.com/300/300'}] as { url: string }[]},
  {id: '456', name: 'Album 456', images: [{url:'http://lorempixel.com/300/300'}] as { url: string }[]},
  // ] as Partial<Album>[]
] as Pick<Album, 'id' | 'name' | 'images'>[]

// type PartialAlbum = Album | Track

// type PartialAlbum = {
//   id: Album['id']
//   name: Album['name']
//   images: Album['images']
// }

// type AlbumKeys = 'id' | 'name' | 'images'

// // Type Map
// type PartialAlbum = {
//   [k in AlbumKeys]: Album[k]
// }

// type PartialAlbum = {
//   [k in keyof Album]?: Album[k]
// }

// type Partial<T> = {
//   [k in keyof T]?: T[k]
// }
