import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'yesno'
})
export class YesnoPipe implements PipeTransform {

  transform(value: boolean, yes = 'yes', no = 'no'): string {
    return value ? yes : no;
  }

}
