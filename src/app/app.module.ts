import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';
import { playlistsMock } from './core/data/playlists.mock';
import { MUSIC_SERVICE_DATA, PLAYLISTS_DATA } from './core/tokens';
import { albumsMock } from './core/data/albums.mock';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    // SuperHiperUltraWidget
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    BrowserAnimationsModule,
    CoreModule
  ],
  providers: [
    {
      provide: PLAYLISTS_DATA,
      useValue: playlistsMock
    },
    {
      provide: MUSIC_SERVICE_DATA,
      useValue: albumsMock
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
