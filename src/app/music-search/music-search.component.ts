import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConnectableObservable, EMPTY, iif, of, ReplaySubject, Subject, Subscription } from 'rxjs';
import { filter, map, multicast, publish, publishReplay, refCount, share, shareReplay, switchMap, takeUntil, tap } from 'rxjs/operators';
import { Album } from '../core/model/album';
import { MusicSearchService } from '../core/services/music-search/music-search.service';

@Component({
  selector: 'app-music-search',
  templateUrl: './music-search.component.html',
  styleUrls: ['./music-search.component.scss']
})
export class MusicSearchComponent implements OnInit {

  query = this.route.queryParamMap.pipe(
    map(q => q.get('q'))
  )

  // globalResults = this.searchService.resultsChanges;

  results = this.query.pipe(
    switchMap(query =>
      iif(() => query != null,
        this.searchService.requestSearchAlbums(query!),
        EMPTY
      )),
    shareReplay()
  )

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private searchService: MusicSearchService) { }

  ngOnInit(): void { }

  searchAlbums(query: string) {
    this.router.navigate([/* '/music' */], {
      relativeTo: this.route,
      queryParams: {
        q: query
      },
    })
  }
}
